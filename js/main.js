"use strict";

//          =====   primary var     =====
var bothanSpies = [];

// var nameCell = document.getElementById('content-name');
// var heightCell = document.getElementById('content-height');
// var massCell = document.getElementById('content-mass');
// var byCell = document.getElementById('content-birthYear');
// var genderCell = document.getElementById('content-gender');
// var homeCell = document.getElementById('content-homePlanet');
// var filmCell = document.getElementById('content-filmsAppeared');

var tableDiv = document.getElementById('tablediv');
var newVar = document.getElementById('target');




//      =====   array of pages      =====
var armedAndFullyOperationalBattlestation = [
    'https://swapi.dev/api/people',
    'https://swapi.dev/api/people/?page=2',
    'https://swapi.dev/api/people/?page=3',
    'https://swapi.dev/api/people/?page=4',
    'https://swapi.dev/api/people/?page=5',
    'https://swapi.dev/api/people/?page=6',
    'https://swapi.dev/api/people/?page=7',
    'https://swapi.dev/api/people/?page=8',
    'https://swapi.dev/api/people/?page=9'
];

// console.log(armedAndFullyOperationalBattlestation);

// var ww = 0;


//      =====   function for the ajax GET       =====
armedAndFullyOperationalBattlestation.forEach(function (Enterprise1701){
    // console.log(Enterprise1701);

    $.ajax(Enterprise1701, {type: "GET"}).done(function (data){
        // console.log(data);
        data.results.forEach(function (entry){
            // console.log(entry)
            bothanSpies.push(entry);
            // console.log("---")
            // console.log(bothanSpies);
            // console.log("---")

        });

        // console.log("outside of foreach, inside of ajax:")
        // console.log(bothanSpies)
        // console.log(bothanSpies.length)

        if (bothanSpies.length === 82){
            console.log("Ah, General Kenobi");
            // console.log(bothanSpies);
            // console.log(bothanSpies[5])




            bothanSpies.forEach(function (entry){
                // console.log("entry.name");
                // console.log(entry.name);
                // console.log("entry");
                // console.log(entry);


                //this is where the fun begins



                var nameHTML = buildHTML(entry.name);
                var heightHTML = buildHTML(entry.height);
                var massHTML = buildHTML(entry.mass);
                var birthYearHTML = buildHTML(entry.birth_year);
                var genderHTML = buildHTML(entry.gender);
                var homePlanetHTML = buildHTMLplanet(entry.homeworld);
                var filmsAppearedHTML = buildHTMLfilm(entry);

                // console.log("!=====");
                // console.log(nameHTML);
                // console.log(heightHTML);
                // console.log(massHTML);
                // console.log(birthYearHTML);
                // console.log(genderHTML);
                // console.log(homePlanetHTML);
                // console.log(filmsAppearedHTML);
                // console.log("#####");
                // console.log(nameCell);
                // console.log(heightCell);
                // console.log(massCell);
                // console.log(byCell);
                // console.log(genderCell);
                // console.log(homeCell);
                // console.log(filmCell);
                // console.log("=====!");
                //
                // console.log("----");
                // console.log($('#content-name').text);
                // console.log($('#content-name').html);
                // console.log(document.getElementById('content-name'));
                // console.log(document.getElementById('content-name').innerHTML);
                // console.log(document.getElementById('content-name').innerText);
                // console.log("----");




                // nameCell.innerHTML += nameHTML;
                // heightCell.innerHTML += heightHTML;
                // massCell.innerHTML += massHTML;
                // byCell.innerHTML += birthYearHTML;
                // genderCell.innerHTML += genderHTML;
                // homeCell.innerHTML += homePlanetHTML;
                // filmCell.innerHTML += filmsAppearedHTML;



                // console.log("*****");
                // console.log("homeworld:");
                // console.log(homePlanetHTML);
                // console.log(entry.homeworld);
                // console.log("=====");
                // console.log("films:");
                // console.log(filmsAppearedHTML);
                // console.log(entry.films);
                // console.log("*****");


                // console.log(nameHTML);
                // console.log(heightHTML);
                // console.log(massHTML);
                // console.log(birthYearHTML);
                // console.log(genderHTML);
                // console.log(homePlanetHTML);
                // console.log(filmsAppearedHTML);


                // newVar.innerHTML += '<div class="row buildArow justify-content-center">';
                // newVar.innerHTML += `<div class="col-1 table-content">${nameHTML}</div>`;
                // newVar.innerHTML += `<div class="col-1 table-content">${heightHTML}</div>`;
                // newVar.innerHTML += `<div class="col-1 table-content">${massHTML}</div>`;
                // newVar.innerHTML += `<div class="col-2 table-content">${birthYearHTML}</div>`;
                // newVar.innerHTML += `<div class="col-1 table-content">${genderHTML}</div>`;
                // newVar.innerHTML += `<div class="col-2 table-content">${homePlanetHTML}</div>`;
                // newVar.innerHTML += `<div class="col-2 table-content">${filmsAppearedHTML}</div>`;
                // newVar.innerText += '</div>';

                tableDiv.innerHTML +=  '<div class="spacer2 col-12"></div>';
                tableDiv.innerHTML += '<div class="col-1 blankdiv"></div>';
                tableDiv.innerHTML += `<div class="col-1 table-content align-middle py-auto">${nameHTML}</div>`;
                tableDiv.innerHTML += `<div class="col-1 table-content align-middle py-auto">${heightHTML}</div>`;
                tableDiv.innerHTML += `<div class="col-1 table-content align-middle py-auto">${massHTML}</div>`;
                tableDiv.innerHTML += `<div class="col-2 table-content align-middle py-auto">${birthYearHTML}</div>`;
                tableDiv.innerHTML += `<div class="col-1 table-content align-middle py-auto">${genderHTML}</div>`;
                tableDiv.innerHTML += `<div class="col-2 table-content align-middle py-auto">${homePlanetHTML}</div>`;
                tableDiv.innerHTML += `<div class="col-2 table-content align-middle py-auto">${filmsAppearedHTML}</div>`;
                tableDiv.innerHTML += '<div class="col-1 blankdiv"></div>';


            })
        } else {
            console.log("Hello There.")
        }

    });

    //the loop counter is going up before the array is complete
    // ww +=1;
    // console.log(ww)
    //
    // console.log(bothanSpies.length);
})



function buildHTML(cell){
    // console.log("cell:");
    // console.log(cell);
    var cellHTML = ''
        cellHTML += '<p class="table-content-text align-middle text-center my-auto">';
    cellHTML += cell;
    cellHTML += '</p>';
    // console.log("cellHTML")
    // console.log(cellHTML)
    return cellHTML;
    }



// function buildHTMLplanet(cell) {
//     console.log(cell);
//
//
//     var cellHTML = '';
//
//     //It's not working
//
//     // $.ajax(cell, {type: "GET"}).done(function (data) {
//     //     // console.log("****")
//     //     // console.log(data.name);
//     //     // console.log("****")
//     //
//     //     cellHTML += `
//     // <p>${data.name}</p>
//     // <div class="spacer2"></div>
//     // `;
//     //
//     //     console.log(`end of GET request cellHTML is ${cellHTML}`)
//     //     // setInterval(function (){
//     //     //     console.log("wait 1000ms")
//     //     // },7000);
//     // });
//
//
//     cellHTML += `
//     <p>${cell}</p>
//     <div class="spacer2"></div>
//     `;
//
//
//     // console.log(`last cellHTML is ${cellHTML}`)
//
//
//
//
//     return cellHTML;
// }

function buildHTMLfilm(cell){
    // console.log(cell.films);
    var filmVar = [];
    var filmVar2 = [];
    // console.log(cell.films);
    cell.films.forEach(function (filmArrayFunction){
        filmVar.push(filmArrayFunction);
    })


    filmVar.forEach(function (episode){
        switch (episode){
            case 'http://swapi.dev/api/films/1/':
                filmVar2.push('The Phantom Menace');
                break;
            case 'http://swapi.dev/api/films/2/':
                filmVar2.push('Attack of the Clones');
                break;
            case 'http://swapi.dev/api/films/3/':
                filmVar2.push('Revenge of the Sith');
                break;
            case 'http://swapi.dev/api/films/4/':
                filmVar2.push('A New Hope');
                break;
            case 'http://swapi.dev/api/films/5/':
                filmVar2.push('The Empire Strikes Back');
                break;
            case 'http://swapi.dev/api/films/6/':
                filmVar2.push('Return of the Jedi');
                break;
            default:
                filmVar2.push(episode);
        }
    });
    // console.log('filmVar:');
    // console.log(filmVar);
    // console.log('filmVar2:');
    // console.log(filmVar2);
    var cellHTML = ''
    cellHTML += '<p class="table-content-text text-center align-middle my-auto">';
    cellHTML += filmVar2.join(', ');
    cellHTML += '</p>';

    // console.log(cellHTML);
    return cellHTML;
}


function buildHTMLplanet(cell){
    // console.log("------")
    // console.log(cell);
    // console.log("------")
    var planetVar = ''




        switch (cell){
            case 'http://swapi.dev/api/planets/1/':
                planetVar += ('Tatooine');
                break;
            case 'http://swapi.dev/api/planets/60/':
                planetVar += ('Umbara');
                break;
            case 'http://swapi.dev/api/planets/12/':
                planetVar += ('Utapau');
                break;
            case 'http://swapi.dev/api/planets/11/':
                planetVar += ('Geonosis');
                break;
            case 'http://swapi.dev/api/planets/51/':
                planetVar += ('Mirial');
                break;
            case 'http://swapi.dev/api/planets/52/':
                planetVar += ('Serenno');
                break;
            case 'http://swapi.dev/api/planets/2/':
                planetVar += ('Alderaan');
                break;
            case 'http://swapi.dev/api/planets/53/':
                planetVar += ('Concord Dawn');
                break;
            case 'http://swapi.dev/api/planets/54/':
                planetVar += ('Zolan');
                break;
            case 'http://swapi.dev/api/planets/55/':
                planetVar += ('Ojom');
                break;
            case 'http://swapi.dev/api/planets/10/':
                planetVar += ('Kamino');
                break;
            case 'http://swapi.dev/api/planets/28/':
                planetVar += ('unknown-28');
                break;
            case 'http://swapi.dev/api/planets/29/':
                planetVar += ('Trandosha');
                break;
            case 'http://swapi.dev/api/planets/30/':
                planetVar += ('Socorro');
                break;
            case 'http://swapi.dev/api/planets/6/':
                planetVar += ('Bespin');
                break;
            case 'http://swapi.dev/api/planets/31/':
                planetVar += ('Mon Cala');
                break;
            case 'http://swapi.dev/api/planets/32/':
                planetVar += ('Chandrila');
                break;
            case 'http://swapi.dev/api/planets/7/':
                planetVar += ('Endor');
                break;
            case 'http://swapi.dev/api/planets/33/':
                planetVar += ('Sullust');
                break;
            case 'http://swapi.dev/api/planets/8/':
                planetVar += ('Naboo');
                break;
            case 'http://swapi.dev/api/planets/36/':
                planetVar += ('Dathomir');
                break;
            case 'http://swapi.dev/api/planets/37/':
                planetVar += ('Ryloth');
                break;
            case 'http://swapi.dev/api/planets/38/':
                planetVar += ('Aleen Minor');
                break;
            case 'http://swapi.dev/api/planets/39/':
                planetVar += ('Vulpter');
                break;
            case 'http://swapi.dev/api/planets/40/':
                planetVar += ('Troiken');
                break;
            case 'http://swapi.dev/api/planets/41/':
                planetVar += ('Tund');
                break;
            case 'http://swapi.dev/api/planets/42/':
                planetVar += ('Haruun Kal');
                break;
            case 'http://swapi.dev/api/planets/18/':
                planetVar += ('Cato Neimoidia');
                break;
            case 'http://swapi.dev/api/planets/9/':
                planetVar += ('Coruscant');
                break;
            case 'http://swapi.dev/api/planets/34/':
                planetVar += ('Toydaria');
                break;
            case 'http://swapi.dev/api/planets/35/':
                planetVar += ('Malastare');
                break;
            case 'http://swapi.dev/api/planets/43/':
                planetVar += ('Cerea');
                break;
            case 'http://swapi.dev/api/planets/44/':
                planetVar += ('Glee Anselm');
                break;
            case 'http://swapi.dev/api/planets/45/':
                planetVar += ('Iridonia');
                break;
            case 'http://swapi.dev/api/planets/47/':
                planetVar += ('Iktotch');
                break;
            case 'http://swapi.dev/api/planets/48/':
                planetVar += ('Quermia');
                break;
            case 'http://swapi.dev/api/planets/49/':
                planetVar += ('Dorin');
                break;
            case 'http://swapi.dev/api/planets/50/':
                planetVar += ('Champala');
                break;
            case 'http://swapi.dev/api/planets/56/':
                planetVar += ('Skako');
                break;
            case 'http://swapi.dev/api/planets/57/':
                planetVar += ('Muunilinst');
                break;
            case 'http://swapi.dev/api/planets/58/':
                planetVar += ('Shili');
                break;
            case 'http://swapi.dev/api/planets/59/':
                planetVar += ('Kalee');
                break;
            case 'http://swapi.dev/api/planets/14/':
                planetVar += ('Kashyyyk');
                break;
            case 'http://swapi.dev/api/planets/21/':
                planetVar += ('Eriadu');
                break;
            case 'http://swapi.dev/api/planets/22/':
                planetVar += ('Corellia');
                break;
            case 'http://swapi.dev/api/planets/23/':
                planetVar += ('Rodia');
                break;
            case 'http://swapi.dev/api/planets/24/':
                planetVar += ('Nal Hutta');
                break;
            case 'http://swapi.dev/api/planets/26/':
                planetVar += ('Bestine IV');
                break;
            case 'http://swapi.dev/api/planets/20/':
                planetVar += ('Stewjon');
                break;
            default:
                planetVar += cell;
        }

    var cellHTML = ''
    cellHTML += '<p class="table-content-text align-middle text-center my-auto">';
    cellHTML += planetVar;
    cellHTML += '</p>';

    return cellHTML;
}




//everything here is happening before the GET requests are done
// $(document).ready
// console.log("outside the function:")
// console.log(bothanSpies);
// console.log(bothanSpies[22]);
// console.log(bothanSpies.length);
// console.log(bothanSpies.forEach(function (wasd){
//     console.log("wasd: ");
//     console.log(wasd);
//     console.log("bothanSpies[wasd]: ");
//     console.log(bothanSpies[wasd]);
// }));